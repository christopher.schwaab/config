call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-sensible'
Plug 'liuchengxu/space-vim-dark'
Plug 'lyuts/vim-rtags'
Plug 'nanotech/jellybeans.vim'
Plug 'ericcurtin/CurtineIncSw.vim'
Plug 'vmchale/ats-vim'
Plug 'mitsuhiko/jinja2'
Plug 'martinda/Jenkinsfile-vim-syntax.git'

call plug#end()

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

let mapleader = " "
" let g:space_vim_dark_background = 233
colorscheme space-vim-dark

set ttyfast
set hlsearch incsearch cursorline
set list
set expandtab ts=4 sw=4
