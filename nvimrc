call plug#begin('~/.config/nvim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'lotabout/skim', { 'dir': '~/.skim', 'do': './install' }
Plug 'liuchengxu/space-vim-dark'
"Plug 'lyuts/vim-rtags'
Plug 'nanotech/jellybeans.vim'
Plug 'ericcurtin/CurtineIncSw.vim'
Plug 'vmchale/ats-vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'rzaluska/deoplete-rtags'
Plug 'zchee/deoplete-jedi'
Plug 'mitsuhiko/jinja2'
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'igankevich/mesonic'
Plug 'tpope/vim-obsession'
Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }
Plug 'peterhoeg/vim-qml'
Plug 'pangloss/vim-javascript'
Plug 'PProvost/vim-ps1'

call plug#end()

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

let mapleader = " "
let g:space_vim_dark_background = 233
" let g:deoplete#enable_at_startup = 1
colorscheme space-vim-dark

set ttyfast
set hlsearch incsearch cursorline
set list
set expandtab softtabstop=2 ts=2 sw=2

" Disable nvimgdb
"let g:loaded_nvimgdb = 1
