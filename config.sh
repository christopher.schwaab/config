#!/bin/bash -xe
CWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

mkdir -p ~/.config/nvim
ln -s ~/.nvimrc ~/.config/nvim/init.vim

ln -s "$CWD/nvimrc" ~/.nvimrc
ln -s "$CWD/tmux.conf" ~/.tmux.conf
ln -s "$CWD/vimrc" ~/.vimrc

ln -s "$CWD/gdb/gdbinit" ~/.gdbinit

mkdir -p ~/.emacs.d/private
for f in "$CWD/spacemacs-private/*"; do
    ln -s "$f" ~/.emacs.d/private
done
