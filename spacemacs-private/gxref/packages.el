(defconst gxref-packages
          '((gxref :location (recipe
                               :fetcher github
                               :repo "dedi/gxref"))))

(defun gxref/init-gxref ()
  (use-package gxref
               :init
               (progn
                 (add-to-list 'xref-backend-functions 'gxref-xref-backend))))
