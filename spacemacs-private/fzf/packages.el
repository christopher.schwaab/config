(defconst fzf-packages
  '((fzf :location (recipe
                     :fetcher github
                     :repo "bling/fzf.el"))))

(defun fzf/init-fzf ()
  (use-package fzf
    :defer 1
    :init
    (progn
      (spacemacs/set-leader-keys
        "off"  'spacemacs/fzf-git
        "ofd"  'spacemacs/fzf-directory
        ))))

;;; packages.el ends here
