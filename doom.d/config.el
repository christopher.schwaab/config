;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Chris Schwaab"
      user-mail-address "christopher.schwaab@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "Fira Code" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
(setq global-so-long-mode 1)

(def-package! deadgrep
  :commands deadgrep)

(def-package! fzf
  :commands (fzf
             fzf-directory
             fzf-git
             fzf-git-files
             fzf-hg
             fzf-projectile
             fzf-git-grep))

(def-package! shx
  :config
  (shx-global-mode 1))

(def-package! vlf
  :config
  (require 'vlf-setup))

(def-package! realgud
  :commands realgud:gdb)

(def-package! realgud-lldb
  :requires realgud
  :commands realgud--lldb)

(after! logview
  (add-to-list 'logview-additional-timestamp-formats '("MarlinTimestamp" "dd/MM/yy HH:mm:ss"))
  (add-to-list 'logview-additional-submodes
               '("Marlin" . ((format . "LEVEL: TIMESTAMP : NAME :")
                             (levels . "RFC 5424")
                             (timestamp . "MarlinTimestamp")))))

(after! projectile
  (setq projectile-project-root-files-top-down-recurring nil)
  (setq projectile-project-root-files-bottom-up '(".projectile")))

(mapc (lambda (m)
        (evil-set-initial-state m 'emacs))
      '(deadgrep-mode
        compilation-mode
        Logview/Marlin))

(map! :leader
      :desc "Clear search highlights" "s c" #'evil-ex-nohighlight
      :desc "Perform a ripgrep search" "s g" #'deadgrep)

(defun epoch-to-time-ap ()
  "Pretty print a unix epoch."
  (interactive)
  (message "%s" (format-time-string "%Y-%m-%d %a %H:%M:%S" (seconds-to-time (string-to-number (read-string "Timestamp to convert: " (thing-at-point 'word)))))))

(defun time-to-epoch-ap ()
  "Convert a pretty time to a unix epoch."
  (interactive)
  (let* ((tenc (apply 'encode-time (parse-time-string (read-string "Time string to convert: " (thing-at-point 'word)))))
         (hi (car tenc))
         (lo (car (cdr tenc))))
    (message "%s" (number-to-string (+ (* hi 65536) lo)))))
