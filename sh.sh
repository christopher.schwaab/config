function vz {
    local res="$([ -z "$1" ] || cd "$1"; fzf --height=50% -m)"
    local relPath
    if [ ! -z "$1" ]; then
        relPath="$1/"
    fi
    if [ ! -z "$res" ]; then
        if [[ "$SHELL" =~ ".*bash$" ]]; then
          history -s nvim "$relPath$res"
        elif [[ "$SHELL" =~ ".*zsh$" ]]; then
          print -s nvim "$relPath$res"
        fi
        nvim "$relPath$res"
    fi
}

alias ff='find . -iname'
alias tmux='TERM=xterm-256color tmux'
alias jsonfmt='python -m json.tool'
alias rp='rg -p'
alias rf='rp --fixed-strings'
alias vim='nvim'
alias nemacs='emacs-26.1 -nw'

alias ls='ls --color'
alias ll='ls -l'
alias la='ll -a'

export EDITOR=nvim
export SVN_EDITOR=$EDITOR
export GIT_PAGER='diff-so-fancy | less --tabs=4 -RFX'
export PS1='$ '
export TERM=screen-256color
